﻿-----------------------------------------------------------------------------------
-- *******************************************************************************
-- * Copyright (c) 2019 Contributors to the Eclipse Foundation
-- *
-- * See the NOTICE file(s) distributed with this work for additional
-- * information regarding copyright ownership.
-- *
-- * This program and the accompanying materials are made available under the
-- * terms of the Eclipse Public License v. 2.0 which is available at
-- * http://www.eclipse.org/legal/epl-2.0.
-- *
-- * SPDX-License-Identifier: EPL-2.0
-- *******************************************************************************
-----------------------------------------------------------------------------------

-- Verteiler technisch - intern ###############################################################################################
DO $$
DECLARE DISTRIBUTION_TEXT TEXT;
BEGIN
  DISTRIBUTION_TEXT := 'Verteiler technisch [TEST]

Sehr geehrte Damen und Herren,

die im Betreff genannte Meldung ist über folgenden Link erreichbar:

$Direkter_Link_zur_Störung$

Mit freundlichen Grüßen
Ihre Admin-Meister-Team der PTA GmbH' ;

UPDATE public.tbl_distribution_group
SET EMAIL_SUBJECT_PUBLISH = 'Die Störung (Sparte: $Sparte$) mit Beginn: $Störungsbeginn_gemeldet$ wurde in den Status veröffentlicht geändert.',
	EMAIL_SUBJECT_UPDATE = 'Die Störung (Sparte: $Sparte$) mit Beginn: $Störungsbeginn_gemeldet$ wurde in den Status aktualisiert geändert.',
	EMAIL_SUBJECT_COMPLETE = 'Die Störung (Sparte: $Sparte$) mit Beginn: $Störungsbeginn_gemeldet$ wurde in den Status beendet geändert.',
    DISTRIBUTION_TEXT_PUBLISH = DISTRIBUTION_TEXT,
	DISTRIBUTION_TEXT_UPDATE = DISTRIBUTION_TEXT,
	DISTRIBUTION_TEXT_COMPLETE = DISTRIBUTION_TEXT
WHERE UUID = '5f5017fe-887a-11ea-bc55-0242ac130003';
END $$;


-- Verteiler fachlich - intern ###############################################################################################
DO $$
DECLARE DISTRIBUTION_TEXT TEXT;
BEGIN
  DISTRIBUTION_TEXT := 'Verteiler fachlich [TEST]

Sehr geehrte Damen und Herren,

die im Betreff genannte Meldung ist über folgenden Link erreichbar:

$Direkter_Link_zur_Störung$

Mit freundlichen Grüßen
Ihre Admin-Meister-Team der PTA GmbH' ;

UPDATE public.tbl_distribution_group
SET EMAIL_SUBJECT_PUBLISH = 'Die Störung (Sparte: $Sparte$) mit Beginn: $Störungsbeginn_gemeldet$ wurde in den Status veröffentlicht geändert.',
	EMAIL_SUBJECT_UPDATE = 'Die Störung (Sparte: $Sparte$) mit Beginn: $Störungsbeginn_gemeldet$ wurde in den Status aktualisiert geändert.',
	EMAIL_SUBJECT_COMPLETE = 'Die Störung (Sparte: $Sparte$) mit Beginn: $Störungsbeginn_gemeldet$ wurde in den Status beendet geändert.',
    DISTRIBUTION_TEXT_PUBLISH = DISTRIBUTION_TEXT,
	DISTRIBUTION_TEXT_UPDATE = DISTRIBUTION_TEXT,
	DISTRIBUTION_TEXT_COMPLETE = DISTRIBUTION_TEXT
WHERE UUID = '5f502046-887a-11ea-bc55-0242ac130003';
END $$;


-- Veröffentlicher ###############################################################################################
UPDATE public.tbl_distribution_group
SET EMAIL_SUBJECT_PUBLISH = 'Bitte Anpassen - Das geplannte Ende der Störung (Sparte: $Sparte$) wird demnächst erreicht. Der Statuswechsel erfolgt automatisch.',
    DISTRIBUTION_TEXT_PUBLISH =
'Veröffentlicher [TEST]

Sehr geehrte Damen und Herren,

die im Betreff genannte Meldung ist über folgenden Link erreichbar:

$Direkter_Link_zur_Störung$

Mit freundlichen Grüßen
Ihre Admin-Meister-Team der PTA GmbH'
WHERE UUID = 'f71660e2-aee1-11ea-b3de-0242ac130004';


-- Test für alle Textbausteine ###############################################################################################
DO $$
DECLARE DISTRIBUTION_TEXT TEXT;
BEGIN
  DISTRIBUTION_TEXT := 'Beschreibung $Beschreibung$
Direkter_Link_zur_Störung $Direkter_Link_zur_Störung$
Druckstufe $Druckstufe$
Hausnummer $Hausnummer$
Interne_Bemerkungen $Interne_Bemerkungen$
Klassifikation $Klassifikation$
Ort $Ort$
Ortsteil $Ortsteil$
Postleitzahl $Postleitzahl$
Radius $Radius$
Spannungsebene $Spannungsebene$
Sparte $Sparte$
Station_Bezeichnung $Station_Bezeichnung$
Status_extern $Status_extern$
Status_intern $Status_intern$
Störungsbeginn_gemeldet $Störungsbeginn_gemeldet$
Störungsende_geplant $Störungsende_geplant$
Störungsende_wiederversorgt $Störungsende_wiederversorgt$
Straße $Straße$
Veröffentlichungsstatus $Veröffentlichungsstatus$
Voraussichtlicher_Grund $Voraussichtlicher_Grund$
Zuständigkeit $Zuständigkeit$' ;
  INSERT INTO public.tbl_distribution_group(UUID, NAME, EMAIL_SUBJECT_PUBLISH, DISTRIBUTION_TEXT_PUBLISH, EMAIL_SUBJECT_UPDATE, DISTRIBUTION_TEXT_UPDATE, EMAIL_SUBJECT_COMPLETE, DISTRIBUTION_TEXT_COMPLETE)
  VALUES('a09140ea-ef06-4773-bb1d-dd7725b89570', 'Test für alle Textbausteine', 'Betreff: Test für alle Textbausteine Veröffentlicht', DISTRIBUTION_TEXT, 'Betreff: Test für alle Textbausteine Aktualisiert', DISTRIBUTION_TEXT,
 'Betreff: Test für alle Textbausteine Beendet', DISTRIBUTION_TEXT);
END $$;
