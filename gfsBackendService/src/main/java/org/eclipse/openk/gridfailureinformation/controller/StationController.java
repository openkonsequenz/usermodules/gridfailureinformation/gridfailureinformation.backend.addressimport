/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.gridfailureinformation.service.StationService;
import org.eclipse.openk.gridfailureinformation.viewmodel.StationDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.StationPolygonDto;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@Log4j2
@RestController
@RequestMapping("/stations")
public class StationController {
    private final StationService stationService;

    public StationController(StationService stationService) {
        this.stationService = stationService;
    }

    @Secured({"ROLE_GRID-FAILURE-ADMIN", "ROLE_GRID-FAILURE-READER", "ROLE_GRID-FAILURE-CREATOR", "ROLE_GRID-FAILURE-QUALIFIER", "ROLE_GRID-FAILURE-PUBLISHER"})
    @Operation(summary = "Anzeige aller Stationen")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Erfolgreich durchgeführt")})
    @GetMapping
    public List<StationDto> findStations() {
        return stationService.getStations();
    }

    @Secured({"ROLE_GRID-FAILURE-ADMIN", "ROLE_GRID-FAILURE-READER", "ROLE_GRID-FAILURE-CREATOR", "ROLE_GRID-FAILURE-QUALIFIER", "ROLE_GRID-FAILURE-PUBLISHER"})
    @Operation(summary = "Holen der Polygon-Koordinaten für die  übergebenen Stationen")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Erfolgreich durchgeführt")})
    @PostMapping("/polygon-coordinates")
    public List<StationPolygonDto> getPolygonCoordinates(
            @Parameter(name="listStationUuids", description="Liste der Stationen, für die gemeinsame Polygon-Koordinaten erstellt werden sollen", required = true)
            @RequestBody List<UUID> listStationUuids) {
        return stationService.getPolygonCoordinates(listStationUuids);
    }

    @Secured({"ROLE_GRID-FAILURE-ADMIN", "ROLE_GRID-FAILURE-READER", "ROLE_GRID-FAILURE-CREATOR", "ROLE_GRID-FAILURE-QUALIFIER", "ROLE_GRID-FAILURE-PUBLISHER"})
    @Operation(summary = "Holen der Anzahl der betroffenen Haushalte")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Erfolgreich durchgeführt")})
    @PostMapping("/count")
    public Long getCountCoordinates(
            @Parameter(name="listStationUuids", description="Liste der Stationen, für die gezählt werden soll", required = true)
            @RequestBody List<UUID> listStationUuids) {
        return stationService.getCountAddresses(listStationUuids);
    }
}
