/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.mapper.tools;

import org.eclipse.openk.gridfailureinformation.config.VisibilityConfig;
import org.eclipse.openk.gridfailureinformation.viewmodel.FESettingsDto;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING, unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface VisibilityConfigurationMapper {
    FESettingsDto.FieldVisibility fromRawToDto(VisibilityConfig.FieldVisibility raw);
    FESettingsDto.TableInternColumnVisibility fromRawToDto(VisibilityConfig.TableInternColumnVisibility raw);
    FESettingsDto.TableExternColumnVisibility fromRawToDto(VisibilityConfig.TableExternColumnVisibility raw);
    FESettingsDto.MapExternTooltipVisibility fromRawToDto(VisibilityConfig.MapExternTooltipVisibility raw);
}
