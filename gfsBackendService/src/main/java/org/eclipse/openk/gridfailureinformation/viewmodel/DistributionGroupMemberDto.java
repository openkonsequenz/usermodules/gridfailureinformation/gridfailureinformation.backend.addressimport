/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.viewmodel;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

@Data
public class DistributionGroupMemberDto implements Serializable {

    @JsonProperty("id")
    private UUID uuid;

    private UUID distributionGroupUuid;
    private String distributionGroup;

    private UUID contactId;
    private String name;
    private String email;
    private String mobileNumber;
    private String mainAddress;

    private String salutationType;
    private String personType;
    private String department;

    private List<String> postcodeList;


}


