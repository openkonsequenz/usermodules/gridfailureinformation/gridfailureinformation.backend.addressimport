/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.importadresses.util;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.LinkedList;
import java.util.List;

@Component
public class UtmConverter {
    @Value("${utm.zonenumber}")
    public double zoneNumber;
    @Value("${utm.zoneletter}")
    public char zoneLetter;

    public List<BigDecimal> convertUTMToDeg(double easting, double northing) {
        double lat;
        double lon;
        double latitude;
        double longitude;

        if (Character.toUpperCase(zoneLetter) <'N') {
            northing -= 10000000;
        }

        lat = (northing/6366197.724/0.9996
                +(1+0.006739496742*Math.pow(Math.cos(northing/6366197.724/0.9996),2)
                -0.006739496742*Math.sin(northing/6366197.724/0.9996)*Math.cos(northing/6366197.724/0.9996)*(Math.atan(Math.cos(Math.atan(( Math.exp((easting - 500000) / (0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(northing/6366197.724/0.9996),2))))*(1-0.006739496742*Math.pow((easting - 500000) / (0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(northing/6366197.724/0.9996),2)))),2)/2*Math.pow(Math.cos(northing/6366197.724/0.9996),2)/3))-Math.exp(-(easting-500000)/(0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(northing/6366197.724/0.9996),2))))*( 1 -  0.006739496742*Math.pow((easting - 500000) / (0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(northing/6366197.724/0.9996),2)))),2)/2*Math.pow(Math.cos(northing/6366197.724/0.9996),2)/3)))/2/Math.cos((northing-0.9996*6399593.625*(northing/6366197.724/0.9996-0.006739496742*3/4*(northing/6366197.724/0.9996+Math.sin(2*northing/6366197.724/0.9996)/2)+Math.pow(0.006739496742*3/4,2)*5/3*(3*(northing/6366197.724/0.9996+Math.sin(2*northing/6366197.724/0.9996 )/2)+Math.sin(2*northing/6366197.724/0.9996)*Math.pow(Math.cos(northing/6366197.724/0.9996),2))/4-Math.pow(0.006739496742*3/4,3)*35/27*(5*(3*(northing/6366197.724/0.9996+Math.sin(2*northing/6366197.724/0.9996)/2)+Math.sin(2*northing/6366197.724/0.9996)*Math.pow(Math.cos(northing/6366197.724/0.9996),2))/4+Math.sin(2*northing/6366197.724/0.9996)*Math.pow(Math.cos(northing/6366197.724/0.9996),2)*Math.pow(Math.cos(northing/6366197.724/0.9996),2))/3))/(0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(northing/6366197.724/0.9996),2))))*(1-0.006739496742*Math.pow((easting-500000)/(0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(northing/6366197.724/0.9996),2)))),2)/2*Math.pow(Math.cos(northing/6366197.724/0.9996),2))+northing/6366197.724/0.9996)))*Math.tan((northing-0.9996*6399593.625*(northing/6366197.724/0.9996 - 0.006739496742*3/4*(northing/6366197.724/0.9996+Math.sin(2*northing/6366197.724/0.9996)/2)+Math.pow(0.006739496742*3/4,2)*5/3*(3*(northing/6366197.724/0.9996+Math.sin(2*northing/6366197.724/0.9996)/2)+Math.sin(2*northing/6366197.724/0.9996 )*Math.pow(Math.cos(northing/6366197.724/0.9996),2))/4-Math.pow(0.006739496742*3/4,3)*35/27*(5*(3*(northing/6366197.724/0.9996+Math.sin(2*northing/6366197.724/0.9996)/2)+Math.sin(2*northing/6366197.724/0.9996)*Math.pow(Math.cos(northing/6366197.724/0.9996),2))/4+Math.sin(2*northing/6366197.724/0.9996)*Math.pow(Math.cos(northing/6366197.724/0.9996),2)*Math.pow(Math.cos(northing/6366197.724/0.9996),2))/3))/(0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(northing/6366197.724/0.9996),2))))*(1-0.006739496742*Math.pow((easting-500000)/(0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(northing/6366197.724/0.9996),2)))),2)/2*Math.pow(Math.cos(northing/6366197.724/0.9996),2))+northing/6366197.724/0.9996))-northing/6366197.724/0.9996)*3/2)*(Math.atan(Math.cos(Math.atan((Math.exp((easting-500000)/(0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(northing/6366197.724/0.9996),2))))*(1-0.006739496742*Math.pow((easting-500000)/(0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(northing/6366197.724/0.9996),2)))),2)/2*Math.pow(Math.cos(northing/6366197.724/0.9996),2)/3))-Math.exp(-(easting-500000)/(0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(northing/6366197.724/0.9996),2))))*(1-0.006739496742*Math.pow((easting-500000)/(0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(northing/6366197.724/0.9996),2)))),2)/2*Math.pow(Math.cos(northing/6366197.724/0.9996),2)/3)))/2/Math.cos((northing-0.9996*6399593.625*(northing/6366197.724/0.9996-0.006739496742*3/4*(northing/6366197.724/0.9996+Math.sin(2*northing/6366197.724/0.9996)/2)+Math.pow(0.006739496742*3/4,2)*5/3*(3*(northing/6366197.724/0.9996+Math.sin(2*northing/6366197.724/0.9996)/2)+Math.sin(2*northing/6366197.724/0.9996)*Math.pow(Math.cos(northing/6366197.724/0.9996),2))/4-Math.pow(0.006739496742*3/4,3)*35/27*(5*(3*(northing/6366197.724/0.9996+Math.sin(2*northing/6366197.724/0.9996)/2)+Math.sin(2*northing/6366197.724/0.9996)*Math.pow(Math.cos(northing/6366197.724/0.9996),2))/4+Math.sin(2*northing/6366197.724/0.9996)*Math.pow(Math.cos(northing/6366197.724/0.9996),2)*Math.pow(Math.cos(northing/6366197.724/0.9996),2))/3))/(0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(northing/6366197.724/0.9996),2))))*(1-0.006739496742*Math.pow((easting-500000)/(0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(northing/6366197.724/0.9996),2)))),2)/2*Math.pow(Math.cos(northing/6366197.724/0.9996),2))+northing/6366197.724/0.9996)))*Math.tan((northing-0.9996*6399593.625*(northing/6366197.724/0.9996-0.006739496742*3/4*(northing/6366197.724/0.9996+Math.sin(2*northing/6366197.724/0.9996)/2)+Math.pow(0.006739496742*3/4,2)*5/3*(3*(northing/6366197.724/0.9996+Math.sin(2*northing/6366197.724/0.9996)/2)+Math.sin(2*northing/6366197.724/0.9996)*Math.pow(Math.cos(northing/6366197.724/0.9996),2))/4-Math.pow(0.006739496742*3/4,3)*35/27*(5*(3*(northing/6366197.724/0.9996+Math.sin(2*northing/6366197.724/0.9996)/2)+Math.sin(2*northing/6366197.724/0.9996)*Math.pow(Math.cos(northing/6366197.724/0.9996),2))/4+Math.sin(2*northing/6366197.724/0.9996)*Math.pow(Math.cos(northing/6366197.724/0.9996),2)*Math.pow(Math.cos(northing/6366197.724/0.9996),2))/3))/(0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(northing/6366197.724/0.9996),2))))*(1-0.006739496742*Math.pow((easting-500000)/(0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(northing/6366197.724/0.9996),2)))),2)/2*Math.pow(Math.cos(northing/6366197.724/0.9996),2))+northing/6366197.724/0.9996))-northing/6366197.724/0.9996 )
        ) * 180/Math.PI;
        latitude=Math.round(lat*10000000);
        latitude=latitude/10000000;

        lon =Math.atan((Math.exp((easting-500000)/(0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(northing/6366197.724/0.9996),2))))*(1-0.006739496742*Math.pow((easting-500000)/(0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(northing/6366197.724/0.9996),2)))),2)/2*Math.pow(Math.cos(northing/6366197.724/0.9996),2)/3))-Math.exp(-(easting-500000)/(0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(northing/6366197.724/0.9996),2))))*(1-0.006739496742*Math.pow((easting-500000)/(0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(northing/6366197.724/0.9996),2)))),2)/2*Math.pow(Math.cos(northing/6366197.724/0.9996),2)/3)))/2/Math.cos((northing-0.9996*6399593.625*( northing/6366197.724/0.9996-0.006739496742*3/4*(northing/6366197.724/0.9996+Math.sin(2*northing/6366197.724/0.9996)/2)+Math.pow(0.006739496742*3/4,2)*5/3*(3*(northing/6366197.724/0.9996+Math.sin(2*northing/6366197.724/0.9996)/2)+Math.sin(2* northing/6366197.724/0.9996)*Math.pow(Math.cos(northing/6366197.724/0.9996),2))/4-Math.pow(0.006739496742*3/4,3)*35/27*(5*(3*(northing/6366197.724/0.9996+Math.sin(2*northing/6366197.724/0.9996)/2)+Math.sin(2*northing/6366197.724/0.9996)*Math.pow(Math.cos(northing/6366197.724/0.9996),2))/4+Math.sin(2*northing/6366197.724/0.9996)*Math.pow(Math.cos(northing/6366197.724/0.9996),2)*Math.pow(Math.cos(northing/6366197.724/0.9996),2))/3)) / (0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(northing/6366197.724/0.9996),2))))*(1-0.006739496742*Math.pow((easting-500000)/(0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(northing/6366197.724/0.9996),2)))),2)/2*Math.pow(Math.cos(northing/6366197.724/0.9996),2))+northing/6366197.724/0.9996))
                *180/Math.PI+zoneNumber*6-183;
        longitude=Math.round(lon*10000000);
        longitude = longitude/10000000;

       List<BigDecimal> decimals = new LinkedList<>();
       decimals.add(BigDecimal.valueOf(longitude).setScale(6, RoundingMode.HALF_EVEN));
       decimals.add(BigDecimal.valueOf(latitude).setScale(6, RoundingMode.HALF_EVEN));

       return decimals;
   }

}
