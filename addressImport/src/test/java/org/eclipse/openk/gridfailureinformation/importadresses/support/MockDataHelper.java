/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.importadresses.support;

import org.eclipse.openk.gridfailureinformation.importadresses.viewmodel.AddressDto;
import org.eclipse.openk.gridfailureinformation.importadresses.viewmodel.StationDto;

import java.math.BigDecimal;
import java.util.UUID;

public class MockDataHelper {

    private MockDataHelper() {}

    public static AddressDto mockAddressDto(int paramToChangeTestObject) {
        AddressDto addressDto = new AddressDto();
        addressDto.setUuid(UUID.randomUUID());
        addressDto.setLatitude(new BigDecimal("12"));
        addressDto.setLongitude(new BigDecimal("33"));
        addressDto.setG3efid(1L);
        addressDto.setDistrict("District_" + paramToChangeTestObject);
        addressDto.setCommunity("Community_" + paramToChangeTestObject);
        addressDto.setHousenumber("15");
        addressDto.setPostcode("51132");
        addressDto.setSdox1(new BigDecimal("1"));
        addressDto.setSdoy1(new BigDecimal("2"));
        addressDto.setGasGroup("GasGroup_" + paramToChangeTestObject);
        addressDto.setWaterGroup("WaterGroup_" + paramToChangeTestObject);
        addressDto.setStationId("StationId_" + paramToChangeTestObject);
        return addressDto;
    }

    public static StationDto mockStationDto(int paramToChangeTestObject) {
        StationDto stationDto = new StationDto();
        stationDto.setUuid(UUID.randomUUID());
        stationDto.setLatitude(new BigDecimal("12"));
        stationDto.setLongitude(new BigDecimal("33"));
        stationDto.setStationName("StationName_" + paramToChangeTestObject);
        stationDto.setG3efid(1L);
        stationDto.setSdox1(new BigDecimal("1"));
        stationDto.setSdoy1(new BigDecimal("2"));
        stationDto.setStationId("StationId_" + paramToChangeTestObject);
        return stationDto;
    }
}
